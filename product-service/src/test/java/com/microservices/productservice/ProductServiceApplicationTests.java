package com.microservices.productservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.microservices.productservice.dto.ProductRequest;
import com.microservices.productservice.dto.ProductResponse;
import com.microservices.productservice.model.Product;
import com.microservices.productservice.repository.ProductRepository;
import com.microservices.productservice.service.ProductService;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.contains;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@Testcontainers
@AutoConfigureMockMvc
class ProductServiceApplicationTests {

    @Container
    static MongoDBContainer mongoDBContainer = new MongoDBContainer("mongo:latest");

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private ProductService productService;

    @DynamicPropertySource
    static void setProps(DynamicPropertyRegistry dynamicPropertyRegistry) {
        dynamicPropertyRegistry.add("spring.data.mongodb.uri", mongoDBContainer::getReplicaSetUrl);
    }

    private ProductRequest getProductRequest(String name, String description, int price) {
        return ProductRequest.builder()
                .name(name)
                .description(description)
                .price(BigDecimal.valueOf(price))
                .build();
    }

    private ProductResponse getProductResponse(String name, String description, int price) {
        return ProductResponse.builder()
                .name(name)
                .description(description)
                .price(BigDecimal.valueOf(price))
                .build();
    }

    @NotNull
    private List<ProductResponse> getProductResponsesMock() {
        List<ProductResponse> productResponses = new ArrayList<>();
        productResponses.add(getProductResponse("prodotto 1", "descrizione prodotto 1", 574));
        productResponses.add(getProductResponse("prodotto 2", "descrizione prodotto 2", 57544));
        productResponses.add(getProductResponse("prodotto 3", "descrizione prodotto 3", 879));
        return productResponses;
    }

    @Test
    void shouldCreateProduct() throws Exception {
        ProductRequest productRequest = getProductRequest("Iphone 13", "ultimo iphone", 456);
        String productRequestString = mapper.writeValueAsString(productRequest);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/product")
                        .content(productRequestString)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        Assertions.assertEquals(1, productService.getAllProducts().size());
    }

    @Test
    void shouldReturnAllProductInDb() throws Exception {
        List<ProductResponse> productResponses = getProductResponsesMock();

        when(productService.getAllProducts()).thenReturn(productResponses);
        String productResponseString = mapper.writeValueAsString(productResponses);

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/api/product")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        assertTrue(mvcResult.getResponse().getContentAsString().equalsIgnoreCase(productResponseString));
    }
}
